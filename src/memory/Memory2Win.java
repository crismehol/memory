package memory;

import java.util.Random;
import javax.swing.JOptionPane;

public class Memory2Win extends javax.swing.JFrame {
    
    int numParejas = 5;
    Random random = new Random();
    String secuencia ="";
    String strNumAleatorio="";
    String secuenciaOculta = "**********";
    char caracterOculto = '*';
    public Memory2Win() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextSecuencia = new javax.swing.JTextField();
        jBComenzar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jTxt_Pos_1 = new javax.swing.JTextField();
        jTxt_Pos_2 = new javax.swing.JTextField();
        jBComprobar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("MEMORY");

        jBComenzar.setText("Comenzar");
        jBComenzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBComenzarActionPerformed(evt);
            }
        });

        jLabel2.setText("Posición 1:");

        jLabel3.setText("Posición 2:");

        jBComprobar.setText("Comprobar");
        jBComprobar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBComprobarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(81, 81, 81)
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextSecuencia, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(jTxt_Pos_1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(jTxt_Pos_2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jBComenzar, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBComprobar))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jTextSecuencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTxt_Pos_1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBComenzar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTxt_Pos_2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBComprobar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBComenzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBComenzarActionPerformed
//        try{ 
            int contadorParejas = 0;

            for(int i=0; i< numParejas*2 ; i++){
                do{
                    // Comienza el contador en 0, genera los num. aleatorios.
                    contadorParejas = 0;
                    int  numAleatorio = random.nextInt(numParejas);
                    strNumAleatorio = String.valueOf(numAleatorio);

                    // Coge y compara los caracteres que se generan.
                    for(int s= 0; s<secuencia.length(); s++){
                        char chSecuencia = secuencia.charAt(s);
                        char chNumAleatorio = strNumAleatorio.charAt(0);

                        // Contador de repeticiones de caracteres.
                        if(chSecuencia == chNumAleatorio){ 
                            contadorParejas++;
                        }
                    }   
                } while(contadorParejas >= 2);           
                secuencia += strNumAleatorio;
            }
            jTextSecuencia.setText(secuencia.replace(secuencia, secuenciaOculta));
//            jTextSecuencia.setText(secuencia);
//        }catch(NumberFormatException ex){ //TIPO DE EXCEPCIÓN.
//            JOptionPane.showMessageDialog(this, "Debe indicar el numero entero\n " + ex.getMessage());           
//        } 
    }//GEN-LAST:event_jBComenzarActionPerformed

    private void jBComprobarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBComprobarActionPerformed
        
        int pos_1 = Integer.valueOf(jTxt_Pos_1.getText());
        int pos_2 = Integer.valueOf(jTxt_Pos_2.getText());
        // Coger caracter de pos_1 --- Coger caracter pos_2
        char caracter_1 = secuencia.charAt(pos_1); 
        char caracter_2 = secuencia.charAt(pos_2);
        secuenciaOculta= "";
        
        for(int i=0; i<secuencia.length(); i++){
            char caracterSecuencia = secuencia.charAt(i);
            // Comparación de posiciones para destapar y de caracteres
            //FIXME: Formar la variable secuenciaOculta con lo que corresponda,
            //  empezando con secuenciaOculta vacía.
            if(i == pos_1 || i == pos_2){                    
                secuenciaOculta+=caracterSecuencia;   
            }else{
                secuenciaOculta+=caracterOculto;
            }
        }    
            // Comparar caracter de 
//             Comparar caracteres --- Mostrar mensaje de comparación
        if(caracter_1 == caracter_2){
            System.out.println("Son iguales ");
        }else{
            System.out.print("No son iguales ");
        }
        jTextSecuencia.setText(secuenciaOculta);
          
    }//GEN-LAST:event_jBComprobarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Memory2Win.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Memory2Win.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Memory2Win.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Memory2Win.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Memory2Win().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBComenzar;
    private javax.swing.JButton jBComprobar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JTextField jTextSecuencia;
    private javax.swing.JTextField jTxt_Pos_1;
    private javax.swing.JTextField jTxt_Pos_2;
    // End of variables declaration//GEN-END:variables
}
